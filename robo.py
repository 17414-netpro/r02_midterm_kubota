# ロボットを定義するクラス 

class Robot(object): 

    def __init__(self, robo): 

        assert isinstance(robo, object) 

        self.rob = robo 

 

    # ロボットにゴミ拾いさせるメソッド 

    def clean(self): 

        # ロボットのAPI呼び出し箇所、現在はダミー 

        print("{0}　お掃除！お掃除！".format(self.rob)) 

 

robo= Robot("鳥羽ロボ") 

robo.clean() 
