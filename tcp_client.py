import socket

ip = '127.0.0.1'
port = 50007

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((ip, port))
    s.sendall(b'hello_17414_kubota')
    data = s.recv(1024)
    print(repr(data))
